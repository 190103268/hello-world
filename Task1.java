import java.util.*;
public class Task1 {
	public static void main(String[] args) {
		Triangle t1 = new Triangle();
		System.out.println(t1.getArea());
	}
}
class Triangle 
		extends GeometricObject {
	private double side1 = 1.0;
	private double side2 = 1.0;
	private double side3 = 1.0;

	public Triangle() {
	}
	public Triangle(double side1, double side2, double side3) {
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
	}
	public double getSide1() {
		return this.side1;
	}
	public double getSide2() {
		return this.side2;
	}
	public double getSide3() {
		return this.side3;
	}
	public void setSide1(double side1) {
		this.side1 = side1;
	}
	public void setSide2(double side1) {
		this.side2 = side2;
	}
	public void setSide3(double side1) {
		this.side3 = side3;
	}
	public double getArea() {
		double halfPerimater = (this.side1 + this.side2 + this.side3)/2.0;
		double area = Math.sqrt(halfPerimater*(halfPerimater - this.side1)*(halfPerimater - side2)*(halfPerimater - side3));
		return area;
	}
	public double getPerimeter() {
		return this.side1 + this.side2 + this.side3;
	}
	public String toString() {
		return "Triangle: side1 = " + this.side1 + " side2 = " + this.side2 + " side3 = " + this.side3;
	}
}
class GeometricObject {
    private String color = " white ";
    private boolean filled;
    private java.util.Date dateCreated;

    public GeometricObject() {
        dateCreated = new java.util.Date();
    }

    public GeometricObject(String color, boolean filled) {
        dateCreated = new java.util.Date();
        this.color = color;
        this.filled = filled;   
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public java.util.Date getDateCreated() {
        return dateCreated;
    }

    public String toString() {
        return "Created on " + dateCreated + "\n color: " + color + " and filled ";                 
    }   
 }
